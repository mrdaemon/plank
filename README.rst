Plank - The anti-forums
=======================
.. Note:: Tentative tagline. It isn't very good. 
          'Plank' is *pure genius* though.

So uh what is this?
--------------------

This is the Python prototype of the Plank project, serves for quick 
experiments and proof of concept code. More information will be added, all you
need to now for now is that it is *awesome* (Perhaps. At least to its authors, 
*some* of whom were even **completely sober** when they thought it over).

More details will populate the wiki eventually.

Prototype and proof of concept code will most likely be Python_ + Flask_.
Final code will perhaps be Haskell_, if the Pythonic half of the team can bring
himself to not brainfart at functional programming and learn a *proper*
language (or so the other half says).

This should be interesting, considering how views on programming among the
team are complete polar opposite. Great occasion to exchange and learn :)

The pythonic half of the team would very much like to mention in passing how
amazing the Flask_ documentation_ is. The world needs more documentation like
this. Seriously.

Very Hastily Written™ Hacker's Guide to Poking at the Source
-------------------------------------------------------------

1. ``$ mkdir ~/.python-envs/``
2. ``$ virtualenv --distribute --no-site-packages ~/.python-envs/plank``
3. ``$ source ~/.python-envs/plank/bin/activate``
4. ``(plank)$ pip install Flask``
5. ``(plank)$ git clone https://github.com/mrdaemon/plank.git``
6. ``(plank)$ cd plank && python boot.py``

You're set, point your browser at http://127.0.0.1:5000/

.. _Python: http://www.python.org/
.. _Flask: http://flask.pocoo.org/
.. _Haskell: http://www.haskell.org/haskellwiki/Haskell
.. _documentation: http://flask.pocoo.org/docs/
