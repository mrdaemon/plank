#!/usr/bin/env python
#        .__                 __
#  _____ |  | _____    ____ |  | __
# \____ \|  | \__  \  /    \|  |/ /
# |  |_> >  |__/ __ \|   |  \    <
# |   __/|____(____  /___|  /__|_ \
# |__|             \/     \/     \/
#
# Plank anti-forum - Version 0.1-experimental
# Copyright (C) Alexandre Gauthier 2010-2011
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#

""" Boot WSGI application """

__author__ = "Alexandre Gauthier <alex@lab.underwares.org>"
__copyright__ = "Copyright 2010-2011, Alexandre Gauthier"
#__credits__ = "Contributions"

__licence__ = "MIT"
__version__ = "0.1-prototype"

from flask import Flask, render_template


# Flask init
app = Flask(__name__)

@app.route('/')
def index():
    """ Page entry point, handles root requests. """
    return render_template('main.html', title="A Plank",
                                        tagline="Hammer some nails in.")

@app.route('/nails/')
def nails_list():
    """ Gives out a main view of all nails."""
    # Prepare a fake object to send to the view
    nails = {'title': 'This is a brilliant question',
             'userhash': '2fd4e1c67a2d28fced849ee1bb76e7391b93eb12',
             'date': 'July 01 1984, 14:48 EST',
             'replies': None,
             'content': 'Oh hai! This is a super data test, or something.'
             }

    return render_template('nails.html', title="A Plank",
                                         tagline="Good use of a hammer.",
                                         nail=nails
                                         )

@app.route('/about')
def about(stat=None):
    """ Controller function greeting the client """
    if stat: print stat
    return 'Plank v %s' % (__version__)

if __name__ == '__main__':
    print "Plank running in development mode. Break to exit."
    app.debug = True
    app.run(host='0.0.0.0')



