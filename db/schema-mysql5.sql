/*        .__                 __
 * ______ |  | _____    ____ |  | __
 * \____ \|  | \__  \  /    \|  |/ /
 * |  |_> >  |__/ __ \|   |  \    <
 * |   __/|____(____  /___|  /__|_ \
 * |__|             \/     \/     \/
 *
 * Plank anti-forum - Version 0.1-experimental
 * Copyright (C) Alexandre Gauthier 2010-2011
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Preliminary Database Schema for MySQL 5.x
 *
 * WARNING: Contains MySQL specific dialect and InnoDB idioms!
 * Probably not very portable as-is.
 */

-- Database Creation and privileges
CREATE DATABASE 'plank' DEFAULT CHARSET=utf8;
GRANT ALL PRIVILEGES ON plank.* TO 'plank'@'localhost' IDENTIFIED BY 'plank'
FLUSH PRIVILEGES

USE 'plank';

-- Identities tables
DROP TABLE IF EXISTS 'identities';
CREATE TABLE 'identities' (
    'id' int(11) NOT NULL auto_increment,
    PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS 'posts';
CREATE TABLE 'identities' (
    'id' int(11) NOT NULL auto_increment,
    PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS 'rules';
CREATE TABLE 'identities' (
    'id' int(11) NOT NULL auto_increment,
    PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS 'stateindex';
CREATE TABLE 'identities' (
    'id' int(11) NOT NULL auto_increment,
    PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
